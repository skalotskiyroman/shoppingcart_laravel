<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::group(['middleware' => 'auth'], function () {

    Route::get('/', [
        'uses' => 'ProductController@index',
        'as' => 'product.index'
    ]);

//adding products to cart

    Route::get('/add-to-cart/{id}/{type}', [
        'uses' => 'CartController@update',
        'as' => 'cart.update'
    ])->where(['id' => '[0-9]+', 'type' => '[\+\-]+']);

    Route::get('/delete/{id}', [
        'uses' => 'CartController@remove',
        'as' => 'cart.remove'
    ]);

    Route::get('/shopping-cart', [
        'uses' => 'CartController@index',
        'as' => 'cart.index'
    ]);
});