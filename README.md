# Shopping cart 
## Getting started

This project is using the following core technologies:

* [PHP Version 7.1.12](http://php.net/) as programming language
* [mysqlnd 5.0.12](https://en.wikipedia.org/wiki/MySQL) / [MariaDB](https://mariadb.org/) as relational database
* [Composer](https://getcomposer.org/) for dependency management in PHP
* [Docker](https://www.docker.com) and [Docker Compose](https://docs.docker.com/compose/) for settings up a personal development environment
* [Laravel 5.4](https://laravel.com/docs/5.4/) framework

### Run application using Docker Compose

You can run this project using docker-compose. 

1.Enter the laradock folder.
```
cd laradock/
```
2.Build the enviroment and run it using docker-compose
```
docker-compose up -d nginx mysql
```
3.Enter the Workspace container, to execute commands like (Artisan, Composer, PHPUnit, Gulp, …)
```$xslt
docker-compose exec workspace bash
```
Alternatively, for Windows PowerShell users: execute the following command to enter any running container:
```$xslt
docker exec -it {workspace-container-id} bash
```
4.Update your project configurations to use the database host
Open your PHP project’s .env file or whichever configuration file you are reading from, and set the database host DB_HOST to mysql:
```$xslt
DB_HOST=mysql
```
### Composer

```$xslt
composer update
```
### Run the migrate command with seed parameter.
```$xslt
php artisan migrate:refresh --seed
```
<br /><br />
## Login and password of test buyer
E-Mail Address: buyer@example.com<br />
Password: buyer