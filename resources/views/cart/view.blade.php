@extends('layouts.master')

@section('title')
    Product in the shopping cart
@endsection

@section('content')
    @if(Session::has('cart'))
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <td class="text-left">Name</td>
                        <td class="text-left">Quantity</td>
                        <td class="text-right">Price</td>
                        <td class="text-right">Total</td>
                        <td class="text-right">Discount (bogof)</td>
                        <td class="text-right"></td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                        <tr>
                            <td class="text-left">{{ $product['item']['name'] }}</td>
                            <td class="text-left">
                                <a href="{{ route('cart.update', ['id' => $product['item']['id'],'type'=>"-"]) }}">&lt;</a>
                                {{ $product['qty'] }}
                                <a href="{{ route('cart.update', ['id' => $product['item']['id'],'type'=>"+"]) }}">&gt;</a>
                            </td>
                            <td class="text-right">{{ $product['item']['price'] }}</td>
                            <td class="text-right">{{ $product['price'] }}</td>
                            <td class="text-right">{{ $product['discount']['oneFree'] }}</td>
                            <td class="text-right">
                                <a href="{{ route('cart.remove', ['product' => $product['item']['id']]) }}">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <strong>"Buy one get one free" discount: {{ $oneFreeDiscount }}</strong>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <strong>10% off on totals greater than £20 (after bogof): {{ $greaterThanDiscount }}</strong>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <strong>2% off on total (after all other discounts) for customers with loyalty cards.: {{ $loyaltyCardsDiscount }}</strong>
            </div>
        </div><br>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <strong>Total price: {{ $totalPrice }}</strong>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <strong>Price after discout: {{ $priceWithDiscount }}</strong>
            </div>
        </div>

    @else
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h2>The are no products in the shopping cart!</h2>
            </div>
        </div>
    @endif
@endsection