<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_buyer = Role::where('name', 'buyer')->first();
        $role_manager  = Role::where('name', 'manager')->first();
        $buyer = new User();
        $buyer->name = 'Buyer Name';
        $buyer->email = 'buyer@example.com';
        $buyer->password = bcrypt('buyer');
        $buyer->one_free = '1';
        $buyer->discount_greater_than = '20';
        $buyer->loyalty_cards = '1';
        $buyer->save();
        $buyer->roles()->attach($role_buyer);
        $manager = new User();
        $manager->name = 'Manager Name';
        $manager->email = 'manager@example.com';
        $manager->password = bcrypt('secret');
        $manager->save();
        $manager->roles()->attach($role_manager);
    }
}
