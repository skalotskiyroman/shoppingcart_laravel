<?php

use Illuminate\Database\Seeder;
use App\Products;
class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = new Products();
        $products->price = "10.2";
        $products->name = 'Product 1';
        $products->description = '';
        $products->save();

        $products = new Products();
        $products->price = "5.60";
        $products->name = 'Product 2';
        $products->description = '';
        $products->save();

        $products = new Products();
        $products->price = "16.80";
        $products->name = 'Product 3';
        $products->description = '';
        $products->save();

        $products = new Products();
        $products->price = "7";
        $products->name = 'Product 4';
        $products->description = '';
        $products->save();
    }
}
