<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Discount;
use App\Products;
use App\Order;
use App\Cart;
use Session;
use Auth;
use DB;


class CartController extends Controller
{
    private $cart;

    public function __construct()
    {
        $this->middleware('auth');

        $this->cart = new Cart();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cartSession = $this->getCartSession();

        if (!Session::has('cart')) {
            return view('cart.view', ['products' => null]);
        }

        return view('cart.view', [
            'oneFreeDiscount' => $cartSession->oneFreeDiscount,
            'greaterThanDiscount' => $cartSession->greaterThanDiscount,
            'loyaltyCardsDiscount' => $cartSession->loyaltyCardsDiscount,
            'products' => $cartSession->items,
            'totalPrice' => $cartSession->totalPrice,
            'priceWithDiscount' => $cartSession->priceWithDiscount
        ]);
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @param  string $type
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $type)
    {
        $cartSession = $this->getCartSession();
        $this->cart->setCart($cartSession);

        $product = Products::find($id);

        switch ($type) {
            case "+":
                $this->cart->add($product, $product->id);
                break;
            case "-":
                $this->cart->deductByOne($product->id);
                break;
            default:
                break;
        }

        $this->updateDbAndSession();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function remove($id)
    {
        $cartSession = $this->getCartSession();
        $this->cart->setCart($cartSession);

        $this->cart->removeItem($id);
        $this->updateDbAndSession();

        return redirect()->back();
    }

    private function getCartSession()
    {

        if (Session::has('cart'))
            return Session::get('cart');

        $cartData = Cart::where('user_id', Auth::user()->id)->first();
        if ($cartData) {
            Session::put('cart', unserialize($cartData->products));
            return Session::get('cart');
        }

        return null;
    }

    private function updateDbAndSession()
    {
        if (count($this->cart->items) > 0) {

            // Set Discount
            $this->calculateDiscount($this->cart);

            Cart::updateOrCreate(
                ['user_id' => Auth::user()->id],
                [
                    'products' => serialize($this->cart),
                    'total' => $this->cart->totalPrice
                ]
            );
            Session::put('cart', $this->cart);
        } else {
            Cart::where('user_id', Auth::user()->id)->delete();

            Session::forget('cart');
        }
    }

    private function calculateDiscount(&$cart)
    {
        $discount = new Discount\Discount();
        $cart = $discount->calc(new Discount\OneFree($cart));
        $cart = $discount->calc(new Discount\GreaterThan($cart));
        $cart = $discount->calc(new Discount\LoyaltyCards($cart));
    }

}
