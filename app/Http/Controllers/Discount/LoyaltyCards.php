<?php

namespace App\Http\Controllers\Discount;

use Session;
use App\Cart;
use Auth;

class LoyaltyCards implements DiscountInterface
{
    private $cart;

    public function __construct(Cart $cart)
    {
        $this->cart = $cart;
    }

    public function calculate()
    {
        $loyaltyCards = Auth::user()->loyalty_cards;

        if ((bool)$loyaltyCards) {
            if (!isset($this->cart->priceWithDiscount)) {
                $this->cart->priceWithDiscount = $this->cart->totalPrice;
            }

            $totalLoyaltyCards = number_format($this->cart->priceWithDiscount * 0.02, 2);
            $this->cart->loyaltyCardsDiscount = $totalLoyaltyCards;
            $this->cart->priceWithDiscount -= $totalLoyaltyCards;
        }
    }

    public function getDiscount()
    {
        return $this->cart;
    }
}
