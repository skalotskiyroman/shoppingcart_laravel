<?php

namespace App\Http\Controllers\Discount;

use App\Cart;

interface DiscountInterface
{
    public function __construct(Cart $cart);
    public function calculate();
    public function getDiscount();
}
