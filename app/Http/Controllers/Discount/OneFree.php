<?php

namespace App\Http\Controllers\Discount;

use Session;
use App\Cart;

class OneFree implements DiscountInterface
{
    private $cart;

    public function __construct(Cart $cart)
    {
        $this->cart = $cart;
    }

    public function calculate()
    {
        $totalOneFree = 0;

        if (!isset($this->cart->priceWithDiscount)) {
            $this->cart->priceWithDiscount = $this->cart->totalPrice;
        }

        foreach ($this->cart->items as $id => $product) {
            if ($product['qty'] < 2) continue;

            $discount = number_format((intdiv($product['qty'], 2)) * $product['item']->price, 2);
            $totalOneFree += $discount;
            $this->cart->items[$id]['discount']['oneFree'] = $discount;
        }
        $this->cart->oneFreeDiscount = $totalOneFree;
        $this->cart->priceWithDiscount -= $totalOneFree;
    }

    public function getDiscount()
    {
        return $this->cart;
    }
}
