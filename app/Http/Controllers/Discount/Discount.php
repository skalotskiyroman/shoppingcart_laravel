<?php

namespace App\Http\Controllers\Discount;

use App\Http\Controllers\Controller;

class Discount
{
    public function calc(DiscountInterface $discount)
    {
        $discount->calculate();
        return $discount->getDiscount();
    }
}
