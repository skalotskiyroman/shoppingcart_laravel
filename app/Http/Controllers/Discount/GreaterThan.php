<?php

namespace App\Http\Controllers\Discount;

use Session;
use App\Cart;
use Auth;

class GreaterThan implements DiscountInterface
{
    private $cart;

    public function __construct(Cart $cart)
    {
        $this->cart = $cart;
    }

    public function calculate()
    {
        $greaterThan = Auth::user()->discount_greater_than;
        
        if (!isset($this->cart->priceWithDiscount)) {
            $this->cart->priceWithDiscount = $this->cart->totalPrice;
        }

        if ($greaterThan < $this->cart->priceWithDiscount) {
            $totalGreaterThan = number_format($this->cart->priceWithDiscount * 0.1, 2);

            $this->cart->greaterThanDiscount = $totalGreaterThan;
            $this->cart->priceWithDiscount -= $totalGreaterThan;
        }
    }

    public function getDiscount()
    {
        return $this->cart;
    }
}
